﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ekranas = new System.Windows.Forms.Label();
            this.mygtukas1 = new System.Windows.Forms.Button();
            this.mygtukas2 = new System.Windows.Forms.Button();
            this.input = new System.Windows.Forms.TextBox();
            this.mygtukas3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ekranas
            // 
            this.ekranas.AutoSize = true;
            this.ekranas.Location = new System.Drawing.Point(209, 168);
            this.ekranas.Name = "ekranas";
            this.ekranas.Size = new System.Drawing.Size(47, 13);
            this.ekranas.TabIndex = 0;
            this.ekranas.Text = "tekstas1";
            this.ekranas.Click += new System.EventHandler(this.label1_Click);
            // 
            // mygtukas1
            // 
            this.mygtukas1.Location = new System.Drawing.Point(212, 335);
            this.mygtukas1.Name = "mygtukas1";
            this.mygtukas1.Size = new System.Drawing.Size(75, 23);
            this.mygtukas1.TabIndex = 1;
            this.mygtukas1.Text = "Paspausk";
            this.mygtukas1.UseVisualStyleBackColor = true;
            this.mygtukas1.Click += new System.EventHandler(this.button1_Click);
            // 
            // mygtukas2
            // 
            this.mygtukas2.Location = new System.Drawing.Point(361, 334);
            this.mygtukas2.Name = "mygtukas2";
            this.mygtukas2.Size = new System.Drawing.Size(75, 23);
            this.mygtukas2.TabIndex = 2;
            this.mygtukas2.Text = "Anuliuoti";
            this.mygtukas2.UseVisualStyleBackColor = true;
            this.mygtukas2.Click += new System.EventHandler(this.button2_Click);
            // 
            // input
            // 
            this.input.Location = new System.Drawing.Point(361, 168);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(130, 20);
            this.input.TabIndex = 3;
            this.input.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // mygtukas3
            // 
            this.mygtukas3.Location = new System.Drawing.Point(385, 218);
            this.mygtukas3.Name = "mygtukas3";
            this.mygtukas3.Size = new System.Drawing.Size(75, 23);
            this.mygtukas3.TabIndex = 4;
            this.mygtukas3.Text = "TextBox";
            this.mygtukas3.UseVisualStyleBackColor = true;
            this.mygtukas3.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 553);
            this.Controls.Add(this.mygtukas3);
            this.Controls.Add(this.input);
            this.Controls.Add(this.mygtukas2);
            this.Controls.Add(this.mygtukas1);
            this.Controls.Add(this.ekranas);
            this.Name = "Form1";
            this.Text = "GrafinePrograma1-1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ekranas;
        private System.Windows.Forms.Button mygtukas1;
        private System.Windows.Forms.Button mygtukas2;
        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.Button mygtukas3;
    }
}

