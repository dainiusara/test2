﻿namespace GrafinePrograma1_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input = new System.Windows.Forms.TextBox();
            this.ekranas = new System.Windows.Forms.Label();
            this.mygtukas1 = new System.Windows.Forms.Button();
            this.mygtukas2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // input
            // 
            this.input.Location = new System.Drawing.Point(53, 54);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(171, 20);
            this.input.TabIndex = 0;
            // 
            // ekranas
            // 
            this.ekranas.AutoSize = true;
            this.ekranas.Location = new System.Drawing.Point(117, 9);
            this.ekranas.Name = "ekranas";
            this.ekranas.Size = new System.Drawing.Size(57, 13);
            this.ekranas.TabIndex = 1;
            this.ekranas.Text = "Rezultatas";
            // 
            // mygtukas1
            // 
            this.mygtukas1.Location = new System.Drawing.Point(53, 93);
            this.mygtukas1.Name = "mygtukas1";
            this.mygtukas1.Size = new System.Drawing.Size(75, 23);
            this.mygtukas1.TabIndex = 2;
            this.mygtukas1.Text = "Skaiciuoti";
            this.mygtukas1.UseVisualStyleBackColor = true;
            this.mygtukas1.Click += new System.EventHandler(this.mygtukas1_Click);
            // 
            // mygtukas2
            // 
            this.mygtukas2.Location = new System.Drawing.Point(148, 92);
            this.mygtukas2.Name = "mygtukas2";
            this.mygtukas2.Size = new System.Drawing.Size(75, 23);
            this.mygtukas2.TabIndex = 3;
            this.mygtukas2.Text = "Anuliuoti";
            this.mygtukas2.UseVisualStyleBackColor = true;
            this.mygtukas2.Click += new System.EventHandler(this.mygtukas2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.mygtukas2);
            this.Controls.Add(this.mygtukas1);
            this.Controls.Add(this.ekranas);
            this.Controls.Add(this.input);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.Label ekranas;
        private System.Windows.Forms.Button mygtukas1;
        private System.Windows.Forms.Button mygtukas2;
    }
}

