﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrafinePrograma1_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void mygtukas1_Click(object sender, EventArgs e)
        {
            int skaicius = Convert.ToInt32(input.Text);

            int kvadratu = skaicius * skaicius;

            ekranas.Text = kvadratu.ToString();
        }

        private void mygtukas2_Click(object sender, EventArgs e)
        {
            ekranas.Text = "Rezultatas";
            input.Text = "";
        }
    }
}
