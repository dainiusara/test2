﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ciklas3
{
    class ciklas3
    {
        static void Main(string[] args)
        {
            SecurityLib.SuperSecure.WhatsNumber(15);
            long i = 1;
            while (i < long.MaxValue)
            {
                if (SecurityLib.SuperSecure.WhatsNumber(i))
                {
                    Console.WriteLine(i);
                }
                Console.ReadLine();
            }
        }
    }
}
